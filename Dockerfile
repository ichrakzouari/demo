FROM openjdk
ADD target/hello-0.0.1-SNAPSHOT.jar hello.war
ENTRYPOINT ["java","-jar","hello.jar"]
EXPOSE 8888
